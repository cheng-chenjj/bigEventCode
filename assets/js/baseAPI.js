$.ajaxPrefilter(function (options) {
  // !options：(Object对象)当前AJAX请求的所有参数选项
  options.url = 'http://big-event-api-t.itheima.net' + options.url;
  // todo 接口文档要求：
  if (options.url.indexOf('/my') !== -1) {
    options.headers = {
      Authorization: localStorage.getItem('bigEventToken') || ''
    };
    options.compelete = (res) => {
      console.log(res);

      if (res.responseJSON.status === 1 && res.responseJSON.message === '身份验证失败!!!') {
        // ******
        // *既然身份验证失败了就可以清除token了，待重新登录拿到token
        // *也可以不清除，新的token会覆盖旧的token
        localStorage.removeItem('bigEventToken');
        // ! clear 会清除所有的 localStorage 项，removeItem() 清除指定项
        // localStorage.clear();

        // *身份验证失败就强行跳转到登录页login.html
        location.href = '/login.html';
      }
    };
  }
});
