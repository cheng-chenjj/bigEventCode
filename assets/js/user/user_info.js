$(function () {
  const form = layui.form;
  const layer = layui.layer;
  // todo 正则校验
  form.verify({
    nickname: function (value) {
      if (value.length > 6) {
        return '昵称长度必须在 1 ~ 6 个字符之间！';
      }
    }
  });

  // todo 初始化用户的基本信息
  initUserInfo();
  function initUserInfo() {
    $.ajax({
      method: 'GET',
      url: '/my/userinfo',
      success: ({ status, message, data }) => {
        if (status !== 0) {
          return layer.msg(message);
        }
        layer.msg(message);
        // **调用form.val()赋值
        form.val('formUserInfo', data);
      }
    });
  }

  // todo 重置
  $('#btnReset').click((e) => {
    e.preventDefault();
    initUserInfo();
  });

  // todo 基本资料表单提交
  $('.layui-form').submit((e) => {
    e.preventDefault();
    // !更新用户信息
    $.ajax({
      method: 'POST',
      url: '/my/userinfo',
      data: $(this).serialize(),
      success: ({ status, message }) => {
        if (status !== 0) return layer.msg(message);
        layer.msg(message);

        // !!子页面调用父级定义好的方法，父级的代码不能被入口函数包裹，需要对外暴露
        window.parent.getUserInfo();
      }
    });
  });
});
