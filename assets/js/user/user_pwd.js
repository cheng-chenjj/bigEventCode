$(function () {
  const form = layui.form;
  const layer = layui.layer;

  // todo 正则
  form.verify({
    pwd: [/^[\S]{6,12}$/, '密码必须6到12位,且不能出现空格'],
    samePwd: function (value) {
      if (value === $('[name=oldPwd]').val()) {
        return '新旧密码不能相同！';
      }
    },
    rePwd: function (value) {
      if (value !== $('[name=newPwd]').val()) {
        return '两次密码不一致！';
      }
    }
  });

  // todo 发起请求实现重置密码的功能
  $('.layui-form').on('submit', function (e) {
    e.preventDefault();
    $.ajax({
      method: 'POST',
      url: '/my/updatepwd',
      data: $(this).serialize(),
      success: ({ status, message }) => {
        if (status !== 0) return layer.msg(message);
        layer.msg(message);
        // **重置表单
        // **reset()是DOM原生的方法
        $('.layui-form')[0].reset();
      }
    });
  });
});
