// $(function () {
const layer = layui.layer;

// todo 获取用户信息
getUserInfo();
function getUserInfo() {
  $.ajax({
    method: 'GET',
    url: '/my/userinfo',
    success: ({ status, message, data }) => {
      console.log(data);
      if (status !== 0) return layer.msg(message);
      // layer.msg(message);
      // 调用渲染函数，传入data
      renderAvatar(data);
    }
  });
}

// todo 渲染头像,昵称
function renderAvatar(info) {
  // !!获取用户名||昵称
  // let uname = info.username || info.nickname;
  // !!使用空值合并运算符改写
  // !!当左侧的操作数为 null 或者 undefined 时，返回其右侧操作数，否则返回左侧操作数。
  let uname = info.username ?? info.nickname;
  // let uname = info.nickname ?? info.username;
  $('#welcome').html(`欢迎${uname}`);

  // !!如果有头像就渲染,没有就取首字母
  if (info.user_pic !== null) {
    $('.layui-nav-img').attr('src', info.user_pic).show();
    $('.text-avatar').hide();
  } else {
    $('.layui-nav-img').hide();
    $('.text-avatar').html(uname[0].toUpperCase()).show();
  }
}

// todo 主页退出功能
$('#logOut').click((e) => {
  e.preventDefault();
  layer.confirm('确定退出？', { icon: 3, title: '提示' }, function () {
    let i = 5;
    let timerId = setInterval(() => {
      layer.msg(`即将在${i}秒后跳转`);
      if (i === 0) {
        clearInterval(timerId);
        // *清除token
        localStorage.removeItem('bigEventToken');
        // *跳转至登录窗口
        location.href = '/login.html';
      }
      i--;
    }, 1000);
  });
});
// });
