$(function () {
  // *注册账号
  $('#link_reg').on('click', function () {
    $('.login-box').hide();
    $('.reg-box').show();
  });

  // *去登录
  $('#link_login').on('click', function () {
    $('.login-box').show();
    $('.reg-box').hide();
  });

  // todo 正则校验开始
  const form = layui.form;
  // ** 轻提示
  const layer = layui.layer;

  form.verify({
    // !pwd的校验规则
    pwd: [/^[\S]{6,12}$/, '密码必须6到12位,且不能出现空格'],
    repwd: function (value) {
      // value是指确认密码框的内容
      const pwd = $('#pwd').val();
      if (pwd !== value) return '两次输入的密码不一致';
    }
  });

  // todo 注册功能
  $('#form_reg').on('submit', (e) => {
    e.preventDefault();
    const username = $('#uname').val();
    const password = $('#pwd').val();
    $.ajax({
      method: 'POST',
      url: '/api/reguser',
      data: { username, password },
      success: ({ status, message }) => {
        console.log(message);
        if (status !== 0) return layer.msg(message);
        layer.msg(message);
        // *对注册成功的用户名和密码本地存储
        localStorage.setItem('idnum', JSON.stringify({ username, password }));
        $('#link_login').click();
      }
    });
    // !!form表单就是用来收集用户信息的，利用ajax去传递数据等待服务器的反馈，button自身是没有submit事件的
    // !!跳转到登录后应该将刚刚注册的用户名和密码进行回写（正常的业务逻辑）
    // !!给button绑定submit事件没用，但是可以用点击事件
  });
  // serialize()会收集所有还有name属性的表单控件的数据
  /*  $('#form_reg').on('submit', (e) => {
    e.preventDefault();
    $.ajax({
      method: 'POST',
      url: '/api/reguser',
      data: $(this).serialize(),
      success: ({ status, message }) => {
        console.log(message);
        if (status !== 0) return layer.msg(message);
      }
    });
  }); */

  // todo 登录功能
  // !登录窗口信息回写
  $('#loginUname').val(JSON.parse(localStorage.getItem('idnum')).username);
  $('#loginpwd').val(JSON.parse(localStorage.getItem('idnum')).password);

  $('#form_login').on('submit', function (e) {
    e.preventDefault();

    $.ajax({
      method: 'POST',
      url: '/api/login',
      data: $(this).serialize(),
      success: ({ status, message, token }) => {
        if (status !== 0) return layer.msg(message);
        layer.msg(message);
        console.log(token);
        localStorage.setItem('bigEventToken', token);

        // *登陆成功后跳转到主页index.html
        location.href = '/index.html';
      }
    });
  });
});
